# WebInt Project
The project Play.it is about creating a web site to handle a shared playlist.

# Main views
## Create queue
This view is about creating a playlist like ** and add at least one music provider.

## Manage queue
This is the core of the app, it handles the next songs with a voting system.

## Add songs / playlist / provider
This let any user contributing to the queue to add
* a song, 
* a playlist, 
* or a music provider.

## Mock API
With help of [schema.org](https://schema.org/MusicPlaylist)

### Queue
An array of songs.
``` json
{
    "name": "Name of the song",
    "author": "Name of the author",
    "image": "URL to the cover",
    "url": "URL to the song",
    "votes": {
        "for": "# of people voting for",
        "total": "total # of people who voted"
    }
}
```

### Provider
An object containing providers with their list of result.
``` json
{
    "search": "Homer",
    "soundcloud": [
        {
            "name": "Homer Said",
            "author": "Dyalla",
            "image": "https://live.staticflickr.com/4384/36531035575_39e13887dd_b.jpg",
            "url": "audio/Homer_Said.mp3"
        }
    ]
}
```

## Expect
* Use element
    * Local storage
    * File
    * Semantic
* Criteria: fix json, no db
* Clean code + valid
* All js (no php)
* All devises
* By the 21/01 + 5-6 slides + 1 report + online (the site)