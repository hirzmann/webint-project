const vote_queue = document.querySelector('#swipe_holder');

const button = document.querySelector('#vote_btn');

let local_vote_list = ['1', '2', '3'];

let x0 = null;
let y0 = null;
let locked_img = false;
let w;
let h;
let rID = null;

let x = 0;
let y = 0;
let open = false;
let animating = false;

function close_vote(event){
    if(open && !vote_queue.contains(event.target)){

        vote_queue.style.setProperty('width', '100px');
        vote_queue.style.setProperty('height', '100px');
        vote_queue.style.setProperty('top',  'calc(100% - 100px)');
        vote_queue.style.setProperty('left', 'calc(50% - 50px)');
        open = false;
    }
}

function open_vote(){
    if(animating){
        setTimeout(open_vote, 50);
        return;
    }
    open = true;
    vote_queue.style.setProperty('top', 'calc(50% - 150px)');
    vote_queue.style.setProperty('left', 'calc(50% - 150px)');
    vote_queue.style.setProperty('width', '300px');
    vote_queue.style.setProperty('height', '300px');

    
    while(vote_queue.firstChild){
        vote_queue.removeChild(vote_queue.firstChild);
    }
    
    local_vote_list.forEach((element, i) => {
        item = document.createElement('div');
        item.innerHTML = element;
        item.style.setProperty('z-index', '-' + i);
        vote_queue.appendChild(item);
    });

    if(vote_queue.firstChild){
        add_listeners(vote_queue.firstChild);
    }


    // e.style.setProperty('display', 'block');
}

function next_queue(){
    if(animating){
        setTimeout(next_queue, 50);
        return;
    }
    vote_queue.removeChild(vote_queue.firstChild);
    local_vote_list.shift();

    if(vote_queue.firstChild){
        add_listeners(vote_queue.firstChild);
    }
}


function unify(e) {	return e.changedTouches ? e.changedTouches[0] : e};

function lock(e) {
    if (open){
        x0 = unify(e).clientX;
        y0 = unify(e).clientY;
        locked_img = true;
    }
 };

function move(e, elem) {
	if(locked_img && open) {
        let dx = unify(e).clientX - x0;
        let dy = unify(e).clientY - y0;

        animating = true;
    
        if (Math.abs(dy) < h/6 && Math.abs(dx) < w/6){
            ani(elem, dx, -dx, dy, -dy, false, 20);
        } else{
            if (dx > 0 && -dy < dx){
                //bottom right
                ani(elem, dx, (w-x0)-dx, dy, (h/2-y0)-dy, true, 20);
              
            } else if(dx < 0 && -dy < Math.abs(dx)){
                //bottom left
                ani(elem, dx, (0-x0)-dx, dy, (h/2-y0)-dy, true, 20);
            } else {
                //top
                ani(elem, dx, (w/2-x0)-dx, dy, (0-y0)-dy, true, 20);
            }
        }


        x = 0;
        y = 0;

        x0 = null;
        y0 = null;
        locked_img = false;
	}
};


function drag(e, elem){
	//e.preventDefault();

	if (locked_img && open){
		let dx = unify(e).clientX - x0;
		let dy = unify(e).clientY - y0;

        elem.style.setProperty('--x', x+dx);
        elem.style.setProperty('--y', y+dy);
	}
}


function size(){
    w = window.innerWidth;
    h = window.innerHeight;
}

function stopAni(){
    animating = false;
	cancelAnimationFrame(rID);
	rID = null;
}

function ease_out(k, e = 1.675){
	 return 1 - Math.pow(1 - k, e)
}

function ani(elem, curx, distX, cury, distY, out, nf, cf=0){

    elem.style.setProperty('--x', curx+distX*ease_out(cf/nf));
    elem.style.setProperty('--y', cury+distY*ease_out(cf/nf));

    if (out) {
        elem.style.setProperty('opacity', 1-ease_out(cf/nf));
    }

    if(cf === nf){
        if(out){
            next_queue();
        }
        stopAni();
        return;
    }

    rID = requestAnimationFrame(ani.bind(this, elem, curx, distX, cury, distY, out, nf, ++cf))
}

size();
addEventListener('resize', size, false);

function add_listeners(elem){
    elem.addEventListener('mousemove', function(event){drag(event, elem)}, false);
    elem.addEventListener('touchmove', function(event){drag(event, elem)}, false);

    elem.addEventListener('mousedown', function(event){lock(event, elem)}, false);
    elem.addEventListener('touchstart', function(event){lock(event, elem)}, false);

    elem.addEventListener('mouseup', function(event){move(event, elem)}, false);
    elem.addEventListener('touchend', function(event){move(event, elem)}, false);

}

document.addEventListener('click', function(event){close_vote(event)}, true);
vote_queue.addEventListener('click', open_vote, false);