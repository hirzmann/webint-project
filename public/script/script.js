

//=========== queue and voting handling ============

const vote_queue = document.querySelector('#swipe_holder');
const vote_back = document.querySelector('#swipe_background');
const playlist = document.querySelector('#playlist');
const searchbar = document.querySelector('#searchbar');
const search_results = document.querySelector('#search-results');


const button = document.querySelector('#vote_btn');

let local_vote_list = ['1', '2', '3'];

let x0 = null;
let y0 = null;
let locked_img = false;
let w;
let h;
let rID = null;

let x = 0;
let y = 0;
let open = false;
let animating = false;

// Adds an song element according to template as child to parent
// Adds song data 
function add_song(parent, template, image, title, artist){
    var temp = document.querySelector(template);
    var song = temp.content.cloneNode(true);
    var img = song.querySelector('img');
    var  content = song.querySelector('.content');
    img.src = image;
    content.children[0].innerHTML = title;
    content.children[1].innerHTML = artist;

    parent.appendChild(song);
}

// loads the global queue into local, simulated with json
function load_queue(){
    fetch("./api/queue.json")
        .then(response  => response.json())
        .then(data => local_vote_list = data)
        .then(update_queue);
}


// loads search results with corresponding data
// Adds listener to add results to queue
function load_search(){
    fetch("./api/search.json")
        .then(response => response.json())
        .then(function(data){
            
            add_song(search_results, "#search_template", 
                                data.soundcloud[0].image,
                                data.soundcloud[0].name, 
                                data.soundcloud[0].author);
            result = search_results.children[0]; 
            result.addEventListener('click', function(){add_to_queue(data)}, false);
        });
    
}

// Loads new results on input in searchbar, clears old results
function search_update(){
    while(search_results.firstChild){
        search_results.removeChild(search_results.firstChild);
    }
    if (searchbar.value != ""){
        load_search();
    }
}

// Adds data to local queue and calls visual update
function add_to_queue(data){
    local_vote_list.push(data.soundcloud[0]);
    update_queue();
}

// updates the html visual queue according to current state of local queue
function update_queue(){
    local_vote_list.forEach((element, i) => {
        item = document.createElement('div');
        item.style.setProperty('background-image', "url("+ element.image + ")");
        item.style.setProperty('z-index', 10-i);
        vote_queue.appendChild(item);
    });
}

// makes voting interface central and ineractable
function open_vote(){
    if(animating){
        setTimeout(open_vote, 50);
        return;
    }
    open = true;
    vote_queue.style.setProperty('top', 'calc(50% - 150px)');
    vote_queue.style.setProperty('left', 'calc(50% - 150px)');
    vote_queue.style.setProperty('width', '300px');
    vote_queue.style.setProperty('height', '300px');

    vote_back.style.setProperty('display', 'inline-block');
    
    while(vote_queue.firstChild){
        vote_queue.removeChild(vote_queue.firstChild);
    }
    
    update_queue();

    if(vote_queue.firstChild){
        add_listeners(vote_queue.firstChild);
    }


    // e.style.setProperty('display', 'block');
}

// shrinks voting interface 
function close_vote(){
    if(open){

        vote_queue.style.setProperty('width', '100px');
        vote_queue.style.setProperty('height', '100px');
        vote_queue.style.setProperty('top',  'calc(100% - 100px)');
        vote_queue.style.setProperty('left', 'calc(50% - 50px)');

        vote_back.style.setProperty('display', 'none');
        open = false;
    }
}

// removes first element of voting queue, if add == true the removed object is added to playlist 
function next_queue(add){
    if(animating){
        setTimeout(next_queue, 50, add);
        return;
    }
    if(add == true){
        song = local_vote_list[0];
        add_song(playlist, "#song_template", song.image, song.name, song.author);
    }

    vote_queue.removeChild(vote_queue.firstChild);
    local_vote_list.shift();

    if(vote_queue.firstChild){
        add_listeners(vote_queue.firstChild);
    }
}

//======= touch/drag interactions ========

// compatibility function
function unify(e) {	return e.changedTouches ? e.changedTouches[0] : e};

// saves location of movement start
// sets variable to indicate further events are part of movement
function lock(e) {
    if (open){
        x0 = unify(e).clientX;
        y0 = unify(e).clientY;
        locked_img = true;
    }
 };

 // handles remaining movement of element when released
 // if movement is above 1/6 of height/ width out to side
 // otherwise reset position
function move(e, elem) {
	if(locked_img && open) {
        let dx = unify(e).clientX - x0;
        let dy = unify(e).clientY - y0;

        animating = true;
    
        if (Math.abs(dy) < h/6 && Math.abs(dx) < w/6){
            ani(elem, dx, -dx, dy, -dy, 0, 20);
        } else{
            if (dx > 0 && -dy < dx){
                //bottom right
                ani(elem, dx, (w-x0)-dx, dy, (h/2-y0)-dy, 1, 20);
              
            } else if(dx < 0 && -dy < Math.abs(dx)){
                //bottom left
                ani(elem, dx, (0-x0)-dx, dy, (h/2-y0)-dy, 2, 20);
                
            } else {
                //top
                ani(elem, dx, (w/2-x0)-dx, dy, (0-y0)-dy, 3, 20);
            }
        }


        x = 0;
        y = 0;

        x0 = null;
        y0 = null;
        locked_img = false;
	}
};

// makes element follow a touch or mouse movement
// triggers background color change according to position
function drag(e, elem){
	//e.preventDefault();

	if (locked_img && open){
		let dx = unify(e).clientX - x0;
		let dy = unify(e).clientY - y0;

        elem.style.setProperty('--x', x+dx);
        elem.style.setProperty('--y', y+dy);
            
        if (dx > 0 && -dy < dx){
            //bottom right
            vote_back.style.setProperty('background-image', 'linear-gradient(to bottom right, rgba(0,0,0,0.4), rgba(0,255,0,' + Math.abs(dx)/w + ')');
        } else if(dx < 0 && -dy < Math.abs(dx)){
            //bottom left
            vote_back.style.setProperty('background-image', 'linear-gradient(to bottom left, rgba(0,0,0,0.4), rgba(255,0,0,' + Math.abs(dx)/w + ')');
        } else {
            //top
            vote_back.style.setProperty('background-image', 'linear-gradient(to top, rgba(0,0,0,0.4), rgba(0,0,255,' + Math.abs(dy)/w + ')');
        }
    }
}

// Cleans up animation end and resets background
function stopAni(){
    animating = false;
    vote_back.style.setProperty('background', 'rgba(0,0,0,0.4)');
	cancelAnimationFrame(rID);
	rID = null;
}

// Animation flow function
function ease_out(k, e = 1.675){
	 return 1 - Math.pow(1 - k, e)
}

// Animation for movement, callbacks itself
function ani(elem, curx, distX, cury, distY, out, nf, cf=0){

    elem.style.setProperty('--x', curx+distX*ease_out(cf/nf));
    elem.style.setProperty('--y', cury+distY*ease_out(cf/nf));

    if (out) {
        elem.style.setProperty('opacity', 1-ease_out(cf/nf));
    }

    if(cf === nf){
        if(out != 0){
            if (out == 1){
                next_queue(true);
            } else {
                next_queue(false);
            }
        }
        stopAni();
        return;
    }

    rID = requestAnimationFrame(ani.bind(this, elem, curx, distX, cury, distY, out, nf, ++cf))
}



function size(){
    w = window.innerWidth;
    h = window.innerHeight;
}

function add_listeners(elem){
    elem.addEventListener('mousemove', function(event){drag(event, elem)}, false);
    elem.addEventListener('touchmove', function(event){drag(event, elem)}, false);

    elem.addEventListener('mousedown', function(event){lock(event, elem)}, false);
    elem.addEventListener('touchstart', function(event){lock(event, elem)}, false);

    elem.addEventListener('mouseup', function(event){move(event, elem)}, false);
    elem.addEventListener('touchend', function(event){move(event, elem)}, false);

}


//========= run at start ===================

size();
load_queue();
addEventListener('resize', size, false);

vote_back.addEventListener('click', function(event){close_vote(event)}, true);
vote_queue.addEventListener('click', open_vote, false);
searchbar.addEventListener('input', search_update, false);



//================= scene change =========

// get elements 
let c = document.getElementsByClassName("nav-current");
let s = document.getElementsByClassName("nav-search");
let q = document.getElementsByClassName("nav-queue");
let current = c[0];
let search = s[0];
let queue = q[0];

// checks if the screen size is small, and starts with "current" state 
if (window.innerWidth <= 768){
    handleSceneChange("current");
    screen.orientation.lock("all");
    screen.lockOrientationUniversal = screen.lockOrientation || screen.mozLockOrientation || screen.msLockOrientation;
}

// functions to change state
function changeToCurrent(){
    if (!current.classList.contains("selected")) {
    current.classList.add("selected");
    search.classList.remove("selected");
    queue.classList.remove("selected");
    handleSceneChange("current")
    }      
}

function changeToQueue(){
    if (!queue.classList.contains("selected")) {
    queue.classList.add("selected");
    search.classList.remove("selected");
    current.classList.remove("selected");
    handleSceneChange("queue")
    }       
}

function changeToSearch(){
    if (!search.classList.contains("selected")) {
    search.classList.add("selected");
    current.classList.remove("selected");
    queue.classList.remove("selected");
    handleSceneChange("search")
    }      
}

// hides elements that aren't selected
function handleSceneChange(id){
    if (id == "current") {
    document.getElementById(id).classList.remove("is-hidden");
    document.getElementById("queue").classList.add("is-hidden");
    document.getElementById("search").classList.add("is-hidden");
    } else if (id == "queue"){
    document.getElementById(id).classList.remove("is-hidden");
    document.getElementById("current").classList.add("is-hidden");
    document.getElementById("search").classList.add("is-hidden");
    } else if (id == "search"){
    document.getElementById(id).classList.remove("is-hidden");
    document.getElementById("queue").classList.add("is-hidden");
    document.getElementById("current").classList.add("is-hidden");

    }
}